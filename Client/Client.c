#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <errno.h>

#define GIVE_REQUESETED_USERNAME "give me requested username!"
#define GIVE_INFORMATION "give me your port!"
#define MULTICAST_IP "239.255.255.250"
#define LOCAL_HOST "127.0.0.1"
#define PENDING_CONNECTIONS 3
#define GAME_OVER "game over"
#define MULTICAST_PORT 1900
#define HIT_MESSAGE "hit\n"
#define ZERO_CHARACTER '0'
#define USERNAME_SIZE 128
#define LISTEN_PORT 2222
#define UNSUCCESSFUL -1
#define NUMBER_BASE 48
#define MSGBUFSIZE 256
#define END_OF_GAME 3
#define NUM_OF_DATA 2
#define BOARD_SIZE 2
#define ONE_KB 1024
#define ONE_KB 1024
#define EXIT "exit"
#define WAIT "wait"
#define ONE_MORE 1
#define PORT 8888
#define DELAY 2
#define FALSE 0
#define BEGIN 0
#define ZERO 0
#define TRUE 1
#define HIT 2
#define TWO 2
#define ONE 1
#define X 0
#define Y 1

char* port = "8888";

int serverfd, got_reply = 1;


int get_socket(int type)
{
	int server_socket;

	if((server_socket = socket(AF_INET, type, ZERO)) == ZERO)
	{
		perror("Socket creation failed!\n");
		exit(UNSUCCESSFUL);
	}
	return server_socket;
}

char** tokenize(char* string, char delimiter)
{
	char** tokens = (char**)malloc(sizeof(char*) * NUM_OF_DATA);
	unsigned int i;

	for (i = 0; i < strlen(string); ++i)
		if (string[i] == delimiter)
			break;

	char* first_part = (char*)malloc(sizeof(char) * i);
	char* second_part = (char*)malloc(sizeof(char) * (strlen(string) - i - 1));

	tokens[ZERO] = first_part;
	tokens[ONE] = second_part;

	for (i = 0; i <= sizeof(first_part); ++i)
		first_part[i] = string[i];

	for (i = i + 1; i < strlen(string); ++i)
		second_part[i - sizeof(first_part) - 2] = string[i];


	printf("first part : %s, second part : %s\n", tokens[ZERO], tokens[ONE]);

	return tokens;
}

void make_the_socket_non_blocking(int socket)
{
	int flags;

	if ((flags = fcntl(socket, F_GETFL, 0)) < 0)
	{
		perror("fcntl error!\n");
		exit(UNSUCCESSFUL);
	}

	if (fcntl(socket, F_SETFL, flags | O_NONBLOCK) < 0)
	{
		perror("fcntl error!\n");
		exit(UNSUCCESSFUL);
	}
}

struct sockaddr_in get_local_server_socket_address(int port)
{
	struct sockaddr_in server_address;

	memset(&server_address, ZERO_CHARACTER, sizeof(server_address));
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(port);

	if(inet_pton(AF_INET, LOCAL_HOST, &server_address.sin_addr) <= ZERO)
	{
		printf("\nInvalid address/ Address not supported \n");
		exit(UNSUCCESSFUL);
	}
	return server_address;
}

void accept_data(int socket_file_descriptor)
{
	struct sockaddr_in cli;
	int len = sizeof(cli);

	if (accept(socket_file_descriptor, (struct sockaddr*)&cli, &len) < 0)
	{
		perror("server acccept failed...\n");
		exit(0);
	}
}

struct sockaddr_in get_server_socket_address(int listen_port)
{
	struct sockaddr_in serv_addr;

	memset(&serv_addr, '0', sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(LISTEN_PORT);

	return serv_addr;
}

void connect_to_sever(struct sockaddr_in serv_addr, int connective_socket)
{
	if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)
	{
		printf("\nInvalid address/ Address not supported \n");
		exit(EXIT_FAILURE);
	}

	if (connect(connective_socket, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
	{
		printf("\nConnection Failed \n");
		exit(EXIT_FAILURE);
	}
}

void set_socket_option(int file_descriptor)
{
	int option = TRUE;
	if(setsockopt(file_descriptor, SOL_SOCKET, SO_REUSEADDR,
			(char*)&option, sizeof(option)) < 0)
	{
		perror("Set socket option failed!\n");
		exit(EXIT_FAILURE);
	}
}

void set_server_socket_option(int file_descriptor)
{
	int option = TRUE;
	if(setsockopt(file_descriptor, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
			(char*)&option, sizeof(option)) < 0)
	{
		perror("Set socket option failed!\n");
		exit(EXIT_FAILURE);
	}
}

void make_the_socket_listener(int server_socket)
{
	if (listen(server_socket, PENDING_CONNECTIONS) < ZERO)
	{
		perror("Listen failed!");
		exit(EXIT_FAILURE);
	}
}

struct sockaddr_in get_socket_address(in_addr_t ip, int port)
{
	struct sockaddr_in address;

	memset(&address, ZERO, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = ip;
	address.sin_port = htons(port);
	return address;
}

void bind_socket(int socket, struct sockaddr_in address, int port)
{
	if (bind(socket, (struct sockaddr*)&address, sizeof(address)) < 0)
	{
		perror("Bind failed!");
		exit(EXIT_FAILURE);
	}
	printf("Listener on port %d \n", port);
}

void set_multicast_options(int file_descriptor)
{
	struct ip_mreq multicast_request;
	multicast_request.imr_multiaddr.s_addr = inet_addr(MULTICAST_IP);
	multicast_request.imr_interface.s_addr = htonl(INADDR_ANY);

	if (setsockopt(file_descriptor, IPPROTO_IP, IP_ADD_MEMBERSHIP,
			(char*)&multicast_request, sizeof(multicast_request)) < 0)
	{
		perror("Setsockopt faild!");
		exit(EXIT_FAILURE);
	}
}

void set_time_value_option(int file_descriptor)
{
	struct timeval time_value;
	time_value.tv_sec = DELAY;
	time_value.tv_usec = DELAY * 1000;
	if (setsockopt(file_descriptor, SOL_SOCKET, SO_RCVTIMEO, &time_value, sizeof(time_value)) < 0)
	{
		perror("Setsockopt faild!");
		exit(EXIT_FAILURE);
	}
}

void connect_to_address(int socket, struct sockaddr_in server_address)
{
	if (connect(socket, (struct sockaddr*)&server_address, sizeof(server_address)) < 0)
	{
		printf("\nConnection Failed \n");
		exit(EXIT_FAILURE);
	}
	printf("Connected!\n");
}

void print_board(char** board)
{
	printf("New board!\n");
	for (int i = BEGIN; i < BOARD_SIZE; ++i)
	{
		for (int j = BEGIN; j < BOARD_SIZE; ++j)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
}

int char_to_int(char c)
{
	return (int)c - NUMBER_BASE;
}

void fill_the_board(char** board)
{
	for (int i = BEGIN; i < BOARD_SIZE; ++i)
	{
		for (int j = BEGIN; j < BOARD_SIZE; ++j)
		{
			board[i][j] = getchar();
			getchar();
		}
		getchar();
	}
}


int is_end_of_game(char** board)
{
	int is_end_of_game = FALSE;

	for (int i = BEGIN; i < BOARD_SIZE; ++i)
		for (int j = BEGIN; j < BOARD_SIZE; ++j)
			is_end_of_game |= ((int)board[i][j] - NUMBER_BASE);

	return !is_end_of_game;
}

int attack(char*** board, char* coordinate)
{
	int x = char_to_int(coordinate[X]);
	int y = char_to_int(coordinate[Y]);

	if (x < 0 | x >= BOARD_SIZE | y < 0 | y >= BOARD_SIZE)
	{
		perror("Invalid coordinate!\n");
		return UNSUCCESSFUL;
	}

	if((*board)[x][y] == '1')
	{
		(*board)[x][y] = '0';
		return HIT;
	}
	return FALSE;
}

char** get_board()
{
	printf("Please enter your board as 2 * 2 matrix :\n");
	char** board = (char**)malloc(sizeof(char*) * BOARD_SIZE);
	for (int i = 0; i < BOARD_SIZE; ++i)
		board[i] = (char*)malloc(sizeof(char) * BOARD_SIZE);

	fill_the_board(board);
	print_board(board);

	return board;
}

int read_from_buffer(char** buffer, int socket_file_descriptor)
{
	puts("Waiting ...");
	bzero((*buffer), sizeof((*buffer)));
	int read_bytes = read(socket_file_descriptor, (*buffer), ONE_KB);
	(*buffer)[read_bytes] = '\0';
	printf("Receiving data : %s", (*buffer));
	if (strncmp(GAME_OVER, (*buffer), sizeof(GAME_OVER)) == 0)
	{
		puts("Server Exit...");
		return END_OF_GAME;
	}

	if (strncmp(HIT_MESSAGE, (*buffer), sizeof(HIT_MESSAGE)) == 0)
	{
		puts("You hit the ship! ... Try another one!");
		return HIT;
	}

	return FALSE;
}

int write_to_buffer(char** buffer, int socket_file_descriptor)
{
	int n = 0;
	bzero((*buffer), sizeof((*buffer)));
	printf("Enter your string :\n");
	while (((*buffer)[n++] = getchar()) != '\n');
	send(socket_file_descriptor, (*buffer), strlen((*buffer)), 0);
	printf("Message sent\n");
}

int play_game(char*** board, char* buffer, int socket_file_descriptor)
{
	int hit = attack(board, buffer);
	print_board((*board));
	if (is_end_of_game((*board)))
	{
		write(socket_file_descriptor, GAME_OVER, sizeof(GAME_OVER));
			return END_OF_GAME;
	}
	return hit;
}

int read_from_buffer_and_play_game(char*** board, char** buffer,
		int socket_file_descriptor)
{
	int game_result = HIT;
	int reading_result;

	while(game_result == HIT)
	{
		reading_result = read_from_buffer(buffer, socket_file_descriptor);

		if (reading_result == END_OF_GAME)
			return END_OF_GAME;
		else if (reading_result == HIT)
			return HIT;

		game_result = play_game(board, (*buffer), socket_file_descriptor);

		if (game_result == END_OF_GAME)
			return END_OF_GAME;
		else if (game_result == UNSUCCESSFUL)
			return UNSUCCESSFUL;
		else if (game_result == HIT)
			send(socket_file_descriptor, HIT_MESSAGE, strlen(HIT_MESSAGE), 0);
	}
	return FALSE;
}

void play_as_host(int socket_file_descriptor)
{
	char** board = (char**)malloc(sizeof(char*) * BOARD_SIZE);
	board = get_board();
	char* buffer = (char*)malloc(sizeof(char) * ONE_KB);

	while (TRUE)
	{
		if (read_from_buffer_and_play_game(&board, &buffer,
				socket_file_descriptor) == END_OF_GAME)
			break;

		write_to_buffer(&buffer, socket_file_descriptor);
	}
}

void play_as_guest(int socket_file_descriptor)
{
	char* buffer = (char*)malloc(sizeof(char) * ONE_KB);
	char** board = (char**)malloc(sizeof(char*) * BOARD_SIZE);
	board = get_board();

	while (TRUE)
	{
		write_to_buffer(&buffer, socket_file_descriptor);

		if (read_from_buffer_and_play_game(&board, &buffer,
				socket_file_descriptor) == END_OF_GAME)
			break;
	}
}

int accept_new_client(struct sockaddr_in address, int server_file_descriptor)
{
	int new_socket;
	int addrlen = sizeof(address);

	if ((new_socket = accept(server_file_descriptor, (struct sockaddr*)&address,
			(socklen_t*)&addrlen)) < 0)
	{
		perror("accept");
		exit(EXIT_FAILURE);
	}
	return new_socket;
}

void wait_for_another_client()
{
	char buffer[ONE_KB + ONE_MORE] = {ZERO};

	int server_file_descriptor = get_socket(SOCK_STREAM);
	set_server_socket_option(server_file_descriptor);
	struct sockaddr_in address = get_socket_address(INADDR_ANY, LISTEN_PORT);
	bind_socket(server_file_descriptor, address, LISTEN_PORT);
	make_the_socket_listener(server_file_descriptor);
	int new_socket = accept_new_client(address, server_file_descriptor);

	play_as_host(new_socket);
	close(new_socket);
}

int connect_to_another_client(int listen_port)
{
	int connective_socket = get_socket(SOCK_STREAM);
	struct sockaddr_in serv_addr = get_server_socket_address(listen_port);
	connect_to_sever(serv_addr, connective_socket);

	play_as_guest(connective_socket);
	close(connective_socket);
}

void check_receiving_data(char* buffer, int socket)
{
	if (strncmp(GIVE_INFORMATION, buffer, sizeof(GIVE_INFORMATION)) == 0)
	{
		char* message = "127.0.0.1:2222";
		puts("Server wants your information! I'll send it!");
		send(socket, message, strlen(message), 0);
		printf("Inforamation sent ...\n");
		wait_for_another_client();
	}
	else
	{
		puts("Server will send you waiting client information!");
		char** tokens = tokenize(buffer, ':');
		connect_to_another_client(atoi(tokens[ONE]));
	}
}

int tcp_handler(char* username)
{
	int valread;
	struct sockaddr_in server_address = get_local_server_socket_address(PORT);
	char buffer[ONE_KB + ONE_MORE] = {ZERO};
	int socket = get_socket(SOCK_STREAM);
	connect_to_address(socket, server_address);
//	make_the_socket_non_blocking(socket);

	send(socket, username, strlen(username), 0);

	bzero(buffer, ONE_KB);
	valread = recv(socket, buffer, ONE_KB, 0);
	puts(buffer);

	check_receiving_data(buffer, socket);

	return 0;
}

void get_heartbeat(struct sockaddr_in heartbeat_address, int file_descriptor)
{
	while (TRUE)
	{
		char msgbuf[MSGBUFSIZE];
		int addrlen = sizeof(heartbeat_address);
		int nbytes = recvfrom(file_descriptor, msgbuf, MSGBUFSIZE, 0,
				(struct sockaddr*)&heartbeat_address, &addrlen);
		if (nbytes < 0)
		{
			perror("recvfrom");
			return;
		}
		msgbuf[nbytes] = '\0';
	 }
}

int main(int argument_counter, char* arguments[])
{
	char c;
	int i = 0;

	printf("Please enter username:requested_username\n");

	char* username = (char*)malloc(sizeof(char) * USERNAME_SIZE);

	while ((c = getchar()) && (c != '\n'))
		username[i++] = c;

//	char* server_broadcast_port = arguments[ONE];
//	char* client_broadcast_port = arguments[TWO];
//	int file_descriptor = get_socket(SOCK_DGRAM);
//	set_socket_option(file_descriptor);
//	struct sockaddr_in heartbeat_address = get_socket_address(
//			htonl(INADDR_ANY), server_broadcast_port);
//	bind_socket(file_descriptor, heartbeat_address, MULTICAST_PORT);
//	set_multicast_options(file_descriptor);
//	set_time_value_option(file_descriptor);

//	if (fork() == 0)
//		get_heartbeat(heartbeat_address, file_descriptor);
//	else
//	{
////		int status;
////		wait(&status);
////		sleep(DELAY);
		tcp_handler(username);
//	}
	return 0;
}
