#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/time.h>

#define GIVE_REQUESETED_USERNAME "give me requested username!"
#define GIVE_INFORMATION "give me your port!"
#define HEARTBEAT_MESSAGE "127.0.0.1:8888"
#define MULTICAST_IP "239.255.255.250"
#define PENDING_CONNECTIONS 3
#define CLIENT_DATA_SIZE 128
#define MULTICAST_PORT 1900
#define MAXIMUM_CLIENTS 30
#define USERNAME_SIZE 128
#define HEARTBEAT_RATE 1
#define UNSUCCESSFUL -1
#define NUM_OF_DATA 2
#define ONE_KB 1024
#define WAIT "wait"
#define ONE_MORE 1
#define PORT 8888
#define EMPTY -1
#define FALSE 0
#define TRUE 1
#define ZERO 0
#define ONE 1
#define TWO 2

void send_heartbeat(int hearbeat_file_descriptor,
		struct sockaddr_in heartbeat_address)
{
	while (TRUE)
	{
		if (sendto(hearbeat_file_descriptor, HEARTBEAT_MESSAGE,
				strlen(HEARTBEAT_MESSAGE), ZERO, (struct sockaddr*)&heartbeat_address,
				sizeof(heartbeat_address)) < ZERO)
		{
			perror("Message sending failed!\n");
			return;
		}
		sleep(HEARTBEAT_RATE);
	}
}

char** tokenize(char* string, char* delimiter)
{
	char** tokens = (char**)malloc(sizeof(char*) * NUM_OF_DATA);
	char* first_part = (char*)malloc(sizeof(char) * USERNAME_SIZE);
	char* second_part = (char*)malloc(sizeof(char) * USERNAME_SIZE);

	first_part = strtok(string, delimiter);
	second_part = strtok(NULL, delimiter);

//	unsigned int i;

//	for (i = 0; i < strlen(string); ++i)
//		if (string[i] == delimiter)
//			break;

//	char* first_part = (char*)malloc(sizeof(char) * i);
//	char* second_part = (char*)malloc(sizeof(char) * (strlen(string) - i - 1));

	tokens[ZERO] = first_part;
	tokens[ONE] = second_part;

//	for (i = 0; i <= sizeof(first_part); ++i)
//		first_part[i] = string[i];

//	for (; i < strlen(string); ++i)
//		second_part[i - sizeof(first_part) - 1] = string[i];

	printf("first part : %s, second part : %s\n", tokens[ZERO], tokens[ONE]);
	return tokens;
}

int get_socket(int type)
{
	int server_socket;

	if((server_socket = socket(AF_INET, type, ZERO)) == ZERO)
	{
		perror("Socket creation failed!\n");
		exit(UNSUCCESSFUL);
	}
	return server_socket;
}

struct sockaddr_in get_socket_address(in_addr_t ip, int port)
{
	struct sockaddr_in address;

	memset(&address, ZERO, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = ip;
	address.sin_port = htons(port);
	return address;
}

void set_socket_option(int server_socket)
{
	int opt = TRUE;
	if(setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR,
			(char*)&opt, sizeof(opt)) < 0)
	{
		perror("Set socket option failed!\n");
		exit(EXIT_FAILURE);
	}
}

void bind_socket(int socket, struct sockaddr_in address)
{
	if (bind(socket, (struct sockaddr*)&address, sizeof(address)) < 0)
	{
		perror("Bind failed!");
		exit(EXIT_FAILURE);
	}
	printf("Listener on port %d \n", PORT);
}

void make_the_socket_listener(int server_socket)
{
	if (listen(server_socket, PENDING_CONNECTIONS) < ZERO)
	{
		perror("Listen failed!");
		exit(EXIT_FAILURE);
	}
}

void clear_socket_set(fd_set* read_file_descriptor, int server_socket)
{
	FD_ZERO(read_file_descriptor);
	FD_SET(server_socket, read_file_descriptor);
}

int get_maximum_socket_descriptor(int* client_sockets, int server_socket ,fd_set* read_file_descriptor)
{
	clear_socket_set(read_file_descriptor, server_socket);

	int client_index, socket_descriptor;
	int maximum_socket_descriptor = server_socket;

	for (client_index = 0; client_index < MAXIMUM_CLIENTS; client_index++)
	{
		socket_descriptor = client_sockets[client_index];
		if (socket_descriptor > 0)
			FD_SET(socket_descriptor, read_file_descriptor);

		if (socket_descriptor > maximum_socket_descriptor)
			maximum_socket_descriptor = socket_descriptor;
	}
	return maximum_socket_descriptor;
}

void initialize_int_array(int** array, int size)
{
	int i;
	for (i = 0; i < size; i++)
		(*array)[i] = ZERO;
}

int accept_new_client(int server_socket,
		struct sockaddr_in* listen_address, socklen_t* address_length)
{
	int new_client_socket;
	if ((new_client_socket = accept(server_socket,
			(__SOCKADDR_ARG)listen_address, address_length)) < ZERO)
	{
		perror("Accept failed!");
		exit(EXIT_FAILURE);
	}
	return new_client_socket;
}

void get_information(int new_client_socket, char** waiting_competitor_info)
{
	printf("Waiting for user data ...\n");
	bzero((*waiting_competitor_info), sizeof((*waiting_competitor_info)));
	recv(new_client_socket, (*waiting_competitor_info), CLIENT_DATA_SIZE, 0);
	printf("user data : %s\n", (*waiting_competitor_info));
}

void send_message(struct sockaddr_in listen_address, int new_client_socket,
		char* message)
{
	printf("New connection, socket descriptor is %d, ip is : %s, port : %d\n",
		   new_client_socket, inet_ntoa(listen_address.sin_addr),
		   ntohs(listen_address.sin_port));

	send(new_client_socket, message, strlen(message), 0);
	puts("Welcome message sent successfully");
}

int match_competitors(int new_client_socket, struct sockaddr_in* listen_address,
		char** waiting_competitor_info)
{
	send_message((*listen_address), new_client_socket, (*waiting_competitor_info));
	bzero((*waiting_competitor_info), sizeof((*waiting_competitor_info)));
	return EMPTY;
}

int wait_for_another_client(int new_client_socket,
		struct sockaddr_in* listen_address, char** waiting_competitor_info)
{
	send_message((*listen_address), new_client_socket, GIVE_INFORMATION);
	get_information(new_client_socket, waiting_competitor_info);
	return new_client_socket;
}

void add_new_client(int client_index, int**client_sockets, int new_client_socket,
		char* username, char*** online_clients)
{
	(*client_sockets)[client_index] = new_client_socket;
	(*online_clients)[client_index] = (char*)malloc(sizeof(char) * strlen(username));
	strcpy((*online_clients)[client_index], username);
	printf("Adding to list of sockets as %d : %s\n", client_index,
			(*online_clients)[client_index]);
}

int check_clients_state(int waiting_for_competitor, int new_client_socket,
		struct sockaddr_in* listen_address, char** waiting_competitor_info,
		char* username, char* requested_username)
{
	if (requested_username == NULL)
	{
		if (waiting_for_competitor == EMPTY)
			waiting_for_competitor = wait_for_another_client(new_client_socket,
					listen_address, waiting_competitor_info);
		else
			waiting_for_competitor = match_competitors(new_client_socket,
					listen_address, waiting_competitor_info);
	}
	return waiting_for_competitor;
}

int manage_new_connection(struct sockaddr_in* listen_address,
		socklen_t* address_length, int** client_sockets, int server_socket,
		int waiting_for_competitor, char** waiting_competitor_info,
		char*** online_clients)
{
	int client_index;
	int new_client_socket = accept_new_client(server_socket,
			listen_address, address_length);

	char* user_data = (char*)malloc(sizeof(char) * USERNAME_SIZE);
	char* username = (char*)malloc(sizeof(char) * USERNAME_SIZE);
	char* requested_username = (char*)malloc(sizeof(char) * USERNAME_SIZE);

	get_information(new_client_socket, &user_data);

	username = strtok(user_data, ":");
	requested_username = strtok(NULL, ":");

	waiting_for_competitor = check_clients_state(waiting_for_competitor,
			new_client_socket, listen_address, waiting_competitor_info,
			username, requested_username);

	for (client_index = 0; client_index < MAXIMUM_CLIENTS; client_index++)
	{
		if((*client_sockets)[client_index] == 0)
		{
			add_new_client(client_index, client_sockets, new_client_socket, username,
						   online_clients);
			return waiting_for_competitor;
		}
	}
}

void disconnect_host(int socket_descriptor, struct sockaddr_in* listen_address,
		int** client_sockets, socklen_t* address_length, int client_index,
		char*** online_clients)
{
	getpeername(socket_descriptor, (struct sockaddr*)listen_address,
			address_length);
	printf("Host disconnected, ip %s, port %d \n", inet_ntoa((*listen_address).sin_addr),
			ntohs((*listen_address).sin_port));

	close(socket_descriptor);
	(*client_sockets)[client_index] = 0;
	free((*online_clients)[client_index]);
}

void manage_io_operation(int** client_sockets, fd_set* read_file_descriptor,
		struct sockaddr_in* listen_address, socklen_t* address_length,
		char*** online_clients)
{
	int client_index, socket_descriptor, bytes_read;
	char buffer[ONE_KB + ONE_MORE];

	for (client_index = 0; client_index < MAXIMUM_CLIENTS; client_index++)
	{
		socket_descriptor = (*client_sockets)[client_index];

		if (FD_ISSET(socket_descriptor, read_file_descriptor))
		{
			if ((bytes_read = read(socket_descriptor, buffer, ONE_KB)) <= 0)
			{
				disconnect_host(socket_descriptor, listen_address, client_sockets,
						address_length, client_index, online_clients);
			}
			else
			{
			}
		}
	}
}

struct sockaddr_in setup_server_socket(int server_socket)
{
	struct sockaddr_in listen_address;

	set_socket_option(server_socket);
	listen_address = get_socket_address(INADDR_ANY, PORT);
	bind_socket(server_socket, listen_address);
	make_the_socket_listener(server_socket);

	return listen_address;
}

void print_clients(char*** online_clients, int** client_sockets)
{
	printf("Clients : \n");
	for (int i = 0; i < MAXIMUM_CLIENTS; ++i)
		printf("Socket : %d, Username : %s\n", (*client_sockets)[i], (*online_clients)[i]);
}

void handle_client_activities(struct sockaddr_in* listen_address, int server_socket,
		int** client_sockets, int* waiting_for_competitor, char** waiting_competitor_info,
		char*** online_clients)
{
//	print_clients(online_clients, client_sockets);

	int address_length = sizeof(*listen_address);
	int activity, maximum_socket_descriptor;
	fd_set read_file_descriptor;

	maximum_socket_descriptor = get_maximum_socket_descriptor(
			*client_sockets, server_socket ,&read_file_descriptor);

	activity = select(maximum_socket_descriptor + ONE_MORE,
			&read_file_descriptor, NULL, NULL, NULL);

	if ((activity < 0) && (errno != EINTR))
	printf("Select error");

	if (FD_ISSET(server_socket, &read_file_descriptor))
		(*waiting_for_competitor) = manage_new_connection(listen_address,
				(socklen_t*)&address_length, client_sockets,
				server_socket, (*waiting_for_competitor),
				waiting_competitor_info, online_clients);

	printf("waiting_for_competitor : %d\n", (*waiting_for_competitor));

	manage_io_operation(client_sockets, &read_file_descriptor, listen_address,
			(socklen_t*)&address_length, online_clients);
}

void manage_clients()
{
	int server_socket = get_socket(SOCK_STREAM);
	struct sockaddr_in listen_address;
	int waiting_for_competitor = EMPTY;
	char* waiting_competitor_info = (char*)malloc(sizeof(char) * CLIENT_DATA_SIZE);
	char** online_clients = (char**)malloc(sizeof(char*) * MAXIMUM_CLIENTS);

	int* client_sockets = (int*)malloc(sizeof(int) * MAXIMUM_CLIENTS);
	initialize_int_array(&client_sockets, MAXIMUM_CLIENTS);

	listen_address = setup_server_socket(server_socket);
	puts("Waiting for connections ...");

	while(TRUE)
		handle_client_activities(&listen_address, server_socket,
				&client_sockets, &waiting_for_competitor,
				&waiting_competitor_info, &online_clients);
}

int get_hearbeat_file_descriptor()
{
	int hearbeat_file_descriptor;

	if ((hearbeat_file_descriptor = socket(AF_INET, SOCK_DGRAM, ZERO)) < ZERO)
	{
		perror("Socket creation failed!\n");
		return UNSUCCESSFUL;
	}
	return hearbeat_file_descriptor;
}

int main(int argument_counter, char* arguments[])
{
	char* server_broadcast_port = arguments[ONE];
	char* client_broadcast_port = arguments[TWO];

	int hearbeat_file_descriptor = get_socket(SOCK_DGRAM);
	struct sockaddr_in heartbeat_address = get_socket_address(
			inet_addr(MULTICAST_IP), atoi(server_broadcast_port));

	if (fork() == ZERO)
		send_heartbeat(hearbeat_file_descriptor, heartbeat_address);
	else
		manage_clients();

	return 0;
}
